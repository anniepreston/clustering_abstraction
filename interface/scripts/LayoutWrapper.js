import GoldenLayout from 'golden-layout';
import { Provider, connect } from 'react-redux';
import TreeViewContainer from './treeView.js';
import ControlsContainer from './controls.js';
import DashboardContainer from './dashboard.js';
import MatchViewContainer from './matchview.js';
import PropTypes from 'prop-types';

class LayoutWrapper extends React.Component {
	componentDidMount(){
		const config = {
			height: 100,
			content: [{
				type: 'row',
				height: 100,
				content: [{
					type: 'stack',
					width: 88,
					content: [
					{
						type: 'react-component',
						title: 'Clusters',
						component: 'TreeViewContainer',
						componentName: 'Merger Trees',
						width: 88	
					},{
						type: 'react-component',
						title: 'DTW',
						component: 'MatchViewContainer',
						componentName: 'DTW',
						width: 88
					}]
				},
				{
					type: 'stack',
					width: 12,
					content: [
					{
						type: 'react-component', 
						title: 'Dashboard',
						component: 'DashboardContainer',
						componentName: 'Dashboard',
						width: 12
					},
					{
						type: 'react-component',
						title: 'Controls',
						component: 'ControlsContainer',
						componentName: 'Controls',
						width: 12
					}
					]
				}]
			}]
		};

		function wrapComponent(Component, store) {
			class Wrapped extends React.Component {
				render() {
					return React.createElement(
						Provider,
						{ store: store },
						React.createElement(Component, this.props)
					);
				}
			}
			return Wrapped;
		};

		var layout = new GoldenLayout(config, this.layout);
		layout.registerComponent('TreeViewContainer', wrapComponent(TreeViewContainer, this.context.store));
		layout.registerComponent('DashboardContainer', wrapComponent(DashboardContainer, this.context.store));
		layout.registerComponent('ControlsContainer', wrapComponent(ControlsContainer, this.context.store));
		layout.registerComponent('MatchViewContainer', wrapComponent(MatchViewContainer, this.context.store));
		layout.init();

		window.addEventListener('resize', () => {
			layout.updateSize();
		});
	}

	render() {
		return React.createElement('div', { className: 'goldenLayout', ref: input => this.layout = input });
	}
}

LayoutWrapper.contextTypes = {
	store: PropTypes.object.isRequired
};

export default LayoutWrapper;