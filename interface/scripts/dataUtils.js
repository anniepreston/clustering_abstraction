import ClusterTree, {Node} from './clusterTree';
import React from 'react';
import * as d3 from 'd3';

var linkageFN;
var similarities = [];
var pairs = [];

export function constructFilenames(p){
	const prefix = '../temp_data';
    var fns = {};
    if (p.domain == 'hacc'){
        linkageFN = prefix + '/hacc/bin_' + p.massBin + '/' + p.comparison + '_comparison/' 
            + p.normalized + '/' + p.linkage + '/linkage';
    }
    else{
    	//not an option right now
        linkageFN = '';//p + 'data/cmip/' + samples + '_samples/' + linkage + '/linkage.csv';
    }
        
    fns.linkage = linkageFN;
    fns.sim = prefix + '/hacc/bin_' + p.massBin + '/sim.csv';
	fns.mass = prefix + '/hacc/bin_' + p.massBin + '/flat_mass.csv';
	fns.time = prefix + '/hacc/bin_' + p.massBin + '/flat_time.csv';
	fns.vel = prefix + '/hacc/bin_' + p.massBin + '/flat_vel.csv';
	fns.xyzPos = prefix + '/hacc/bin_' + p.massBin + '/flat_xyzpos.csv';
	fns.xyzVel = prefix + '/hacc/bin_' + p.massBin + '/flat_xyzvel.csv';

    fns.mergerTimes = prefix + '/hacc/bin_' + p.massBin + '/merger_times.csv';
    fns.mergerRatios = prefix + '/hacc/bin_' + p.massBin + '/merger_ratios.csv';
    fns.mergedMasses = prefix + '/hacc/bin_' + p.massBin + '/merged_masses.csv';

	/*
	fns.lat = prefix + 'data/cmip/' + samples + '_samples/t_' + timesteps + '_lat.csv';
	fns.lon = prefix + 'data/cmip/' + samples + '_samples/t_' + timesteps + '_long.csv';
	fns.t0 = prefix + 'data/cmip/' + samples + '_samples/t_' + timesteps + '_T0.csv';
	fns.t33 = prefix + 'data/cmip/' + samples + '_samples/t_' + timesteps + '_T33.csv';
	fns.tf = prefix + 'data/cmip/' + samples + '_samples/t_' + timesteps + '_TF.csv';
	fns.tg = prefix + 'data/cmip/' + samples + '_samples/t_' + timesteps + '_TG.csv';
	*/

    return fns;
}

export function loadLinkage(filename){
    return $.ajax({
        type: "GET",
        url: linkageFN,
        success: function(data) {
        }
    })
}

export function parseLinkage(data){
    const dataArray = $.csv.toArrays(data[0]);
    var linkage = storeLinkage(dataArray);
    var tree = createTree(dataArray);
    return ({linkage: linkage, tree: tree});
}

function storeLinkage(linkage){
	for (var i = 0; i < linkage.length; i++){
		similarities[i] = parseFloat(linkage[i][2]);
		pairs[i] = [parseFloat(linkage[i][0]), parseFloat(linkage[i][1])];
	}
    return {similarities: similarities, pairs: pairs};
}

export function loadData(filename){
    return $.ajax({
        type: "GET",
        url: filename,
        success: function(data) {
        }
    })
}

export function parseData(name, data){
    const array = $.csv.toArrays(data[0]);
    var floatArray = [];
    for (var i = 0; i < array.length; i++){
        var row = [];
        for (var j = 0; j < array[i].length; j++){
            if (name[0] == 'x' && name[1] == 'y' && name[2] == 'z'){
                var string = array[i][j];
                string = string.substring(1,string.length-1)
                string = string.split(' ');
                var stringArray = [];
                for (var s = 0; s < string.length; s++){
                    if (string[s] != ''){
                        stringArray.push(string[s]);
                    }
                }
                var x = parseFloat(stringArray[0]), y = parseFloat(stringArray[1]), z = parseFloat(stringArray[2]);
                if (name == 'xyzPos'){ //position wrapping adjustment
                    if (x > 150) x = x-256;
                    if (y > 150) y = y-256;
                    if (z > 150) z = z-256;
                }
                row.push([x, y, z]);
            }
            else { row.push(parseFloat(array[i][j])); }
        }
        floatArray.push(row);
    }
    return floatArray;
}

function createTree(linkage){
    var tree = new ClusterTree();
	var n  = linkage.length;
	tree.nInputs = n+1;
	tree.root = new Node(2*n);
	for (var l = 0; l < n; l++){
		var idx = n-1-l;
		var a = linkage[idx][0];
		var b = linkage[idx][1];
		tree.insert({a, b}, (2*n)-l, l);
	}
    return tree;
}

export function getMainBranch(time, mass, data){
	var newData = [];
	const indices = getMainBranchIDs(time, mass);
	for (var i = 0; i < indices.length; i++){
		var dataRow = [];
		const IDs = indices[i];
		for (var j = 0; j < IDs.length; j++){
			dataRow.push(data[i][IDs[j]]);
		}
		newData.push(dataRow);	
	}
	return newData;
}

function addMonths(){
    var months = timesteps.split('_');
    var monthArray = [];
    for (i = 0; i < timeSeries["t0"].length; i++){
        monthRow = [];
        for (m = parseInt(months[0]); m < parseInt(months[1]); m++){
	        monthRow.push(m);
	    }
	    monthArray.push(monthRow);
	}
	fillTimeSeries("month", monthArray);
}

function fillTimeSeries(name, floatArray){
	timeSeries[name] = floatArray;
}

function getMainBranchIDs(time, mass){
	// given flattened tree data, extract main masses (using mass array & time array)
	var timeArrays = time;
	var massArrays = mass;
	var flatIndexArrays = [];
	//for each array in arrays,
	for (var i = 0; i < timeArrays.length; i++){
		var t = 0;
		//for each timestep,
		var flatIndexArray = []
		while(t <= Math.max.apply(Math, timeArrays[i])){
			var indicesForT = []; // all indices that = this timestep
			for (var j = 0; j < timeArrays[i].length; j++){
				if (timeArrays[i][j] == t) { indicesForT.push(j); }
			}
			var maxID = -1;
			var maxMass = 0.0;
			for (var idx = 0; idx < indicesForT.length; idx++){
				var m = massArrays[i][indicesForT[idx]];
				if (m > maxMass) { 
					maxMass = m; maxID = indicesForT[idx]; 
				}
			}
			if (maxID != -1) { flatIndexArray.push(maxID); }
			t++;
		}
		if (flatIndexArray.length > 0){ flatIndexArrays.push(flatIndexArray); }
	}
	return flatIndexArrays;
}

export function calculateClusterRibbons(p){ 
    console.log("calculating cluster ribbons..."); 
    const nClusters = p.nClusters;
    const clusterIDs = p.clusterAssignments;  
    const timeSeries = p.timeSeries;
    var dataStats = {};
    var haccVars = ["mainMass", "mainVel", "mergers"];
    var usgsVars = ["t0", "t33", "tf", "tg"];
    var timeString;
    if (p.domain == 'usgs') { timeString = "month"; }
    else { timeString = "mainTime"; }
    var valuesPerTimestep = {};
    var varNames;
    if (p.domain == 'usgs') { varNames = usgsVars; }
    else varNames = haccVars;

    // for each timestep, get an array of values (the lookup is different for the merger times...)
    for (var v = 0; v < varNames.length; v++){
        valuesPerTimestep[varNames[v]] = {};
        for (var t = 0; t <= Math.max.apply(Math, timeSeries[timeString][0]); t++){
            valuesPerTimestep[varNames[v]][t] = {};
            for (var n = 0; n < nClusters; n++){ 
                valuesPerTimestep[varNames[v]][t][n] = [];
            }
        }
        var length = 0;
        if (varNames[v] == 'mergers') length = timeSeries['mergerRatios'].length;
        else length = timeSeries[varNames[v]].length;
        for (var i = 0; i < length; i++){
            if (varNames[v] == 'mergers'){
                for (var t = 0; t < timeSeries['mergerTimes'][i].length; t++){
                    var time = timeSeries['mergerTimes'][i][t];
                    var mass_value = timeSeries['mergedMasses'][i][t];
                    var ratio_value = timeSeries['mergerRatios'][i][t];
                    var clusterID = clusterIDs[i];
                    if (typeof mass_value !== "undefined") { 
                        valuesPerTimestep[varNames[v]][time][clusterID].push({'mass': mass_value, 'ratio': ratio_value}); 
                    }
                }
            }
            else {
                for (var t = 0; t < timeSeries[timeString][i].length; t++){
                    var time = timeSeries[timeString][i][t];
                    var value = timeSeries[varNames[v]][i][t];
                    var clusterID = clusterIDs[i];
                    if (typeof value !== "undefined") { valuesPerTimestep[varNames[v]][time][clusterID].push(value); }  
                }
            }
        }
    }
         
    for (var v = 0; v < varNames.length; v++){
        dataStats[varNames[v]] = {};
        for (var n = 0; n < nClusters; n++){ 
            dataStats[varNames[v]][n] = {}; //create empty object for each cluster
            var max_idxs = [];
            for (var t = 0; t <= Math.max.apply(Math, timeSeries[timeString][0]); t++){
                var array = valuesPerTimestep[varNames[v]][t][n];
                dataStats[varNames[v]][n][t] = {};
                var maxIndex = -1;
                var max = 0;
                for (var i = 0; i < array.length; i++) {
                    var value = array[i];
                    if (varNames[v] == 'mergers') value = array[i].ratio;
                    if (value > max) {
                        maxIndex = i;
                        max = value;
                    }
                }
                if (maxIndex != -1) max_idxs.push(maxIndex);
            }

            function mode(arr){
                return arr.sort((a,b) =>
                    arr.filter(v => v===a).length
                    - arr.filter(v => v===b).length
                ).pop();
            }
            const outlier_idx = mode(max_idxs);
            for (var t = 0; t <= Math.max.apply(Math, timeSeries[timeString][0]); t++){
                var array = valuesPerTimestep[varNames[v]][t][n];
                var outlier = array[outlier_idx];
                if (varNames[v] == 'mainMass') var outlier_vel = valuesPerTimestep['mainVel'][t][n][outlier_idx];
                if (varNames[v] != 'mergers') {
                    array.splice(outlier_idx, 1);
                    var min = Math.min.apply(Math, array);
                    var max = Math.max.apply(Math, array);
                    var lower = getQuartile(array, 25);
                    var upper = getQuartile(array, 75);
                    if (typeof max != "undefined"){ dataStats[varNames[v]][n][t].max = max; }
                    if (typeof min != "undefined"){ dataStats[varNames[v]][n][t].min = min; }
                    if (typeof lower != "undefined"){ dataStats[varNames[v]][n][t].lower = lower; }
                    if (typeof upper != "undefined"){ dataStats[varNames[v]][n][t].upper = upper; }
                    if (typeof array !== "undefined"){ dataStats[varNames[v]][n][t].med = Median(array); }
                    if (typeof array !== "undefined"){ dataStats[varNames[v]][n][t].out = outlier; }
                    if (typeof array !== "undefined" && varNames[v] == 'mainMass'){ dataStats[varNames[v]][n][t].out_vel = outlier_vel; }
                }
                else {
                    //bin by mass
                    if (array.length > 0){
                        var histogram = d3.histogram()
                            .thresholds(8)
                            .value(d => d.mass);
                        var bins = histogram(array);
                        for (var bin = 0; bin < bins.length; bin++){
                            dataStats[varNames[v]][n][t][bin] = {};
                            var ratios = [];
                            var masses = [];
                            for (var i = 0; i < bins[bin].length; i++){
                                ratios.push(bins[bin][i].ratio);
                                masses.push(bins[bin][i].mass);
                            }
                            if (masses.length > 0){
                                var min = Math.min.apply(Math, ratios);
                                var max = Math.max.apply(Math, ratios);
                                var lower = getQuartile(ratios, 25);
                                var upper = getQuartile(ratios, 75);
                                if (typeof max != "undefined"){ dataStats[varNames[v]][n][t][bin].max = max; }
                                if (typeof min != "undefined"){ dataStats[varNames[v]][n][t][bin].min = min; }
                                if (typeof lower != "undefined"){ dataStats[varNames[v]][n][t][bin].lower = lower; }
                                if (typeof upper != "undefined"){ dataStats[varNames[v]][n][t][bin].upper = upper; }
                                if (typeof ratios !== "undefined"){ dataStats[varNames[v]][n][t][bin].med = Median(ratios); }
                                if (typeof masses != 'undefined'){ dataStats[varNames[v]][n][t][bin].mass = Median(masses); }
                            }
                        }
                    }
                }
            }
        }
    }
    
    function Median(array){
        array.sort(function(a,b){
            return a-b;
        });
        if (array.length === 0) return 0;

        var halfID = Math.floor(array.length/2);

        if (array.length % 2) return array[halfID];
        else return (array[halfID - 1] + array[halfID]) / 2.0;
    }

    function getQuartile(array, n){
        const median = Median(array);
        if (n == 25){
            var half = array.filter(function(f){ return f < median; });
        }
        else if (n == 75){
            var half = array.filter(function(f){ return f >= median; });
        }
        return Median(half);
    }

    return dataStats;
}

function _updateNClusters(n){
    clusterAssignments = tree.getClusterAssignments(n);
    clusterView.setNClusters(n);
            
	var dataStats = calculateClusterRibbons(n, clusterAssignments);
	
	clusterView.drawClusters(dataStats, encodingOption);
	
	clusterView.drawTimeSeries(timeSeries, timeseriesOption, clusterAssignments);
	
	dashboard.update(n, timeSeries, clusterAssignments, this.dashOption);
}

function updateTimeseries(option){
    clusterView.drawTimeSeries(timeSeries, option, clusterAssignments);
}

function updateLinkage(option, n){
	if (p.domain == 'hacc'){
        linkageFN = 'data/hacc/bin_' + massBin + '/' + comparison + '_comparison/' 
            + normalized + '/' + option + '/linkage';
    }
    else{ linkageFN = 'data/cmip/' + samples + '_samples/' + option + '/linkage.csv'; }
	clusterView.setNClusters(n);
	
	$.when(loadLinkage(filenames.linkage)).then(function(){

		clusterAssignments = tree.getClusterAssignments(n);
	
		var dataStats = calculateClusterRibbons(n, clusterAssignments);
		clusterView.drawClusters(dataStats, encodingOption);
	
		clusterView.drawTimeSeries(timeSeries, timeseriesOption, clusterAssignments);
	
		dashboard.update(n, timeSeries, clusterAssignments, this.dashOption);
	
	});
}

function updateEncoding(option, n){
	this.encodingOption = option;
	clusterView.drawClusters(dataStats, option);
}

function updateDash(option, n){
	this.dashOption = option;
	this.nClusters = n;
	dashboard.update(n, timeSeries, clusterAssignments, option);
}