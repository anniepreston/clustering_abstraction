import { getClusterAssignments } from './clusterTree';
import { constructFilenames, loadLinkage, parseLinkage, loadData, parseData, getMainBranch, calculateClusterRibbons } from './dataUtils';
import { Map } from 'immutable';

import React, { Component } from 'react';
import ReactDOM, {render} from 'react-dom';
import {connect} from 'react-redux';

//import * as fs from 'fs';

export const FETCH_DATA_BEGIN = 'FETCH_DATA_BEGIN';
export const FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS';
export const FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE';
export const UPDATE_N_CLUSTERS = 'UPDATE_N_CLUSTERS';
export const UPDATE_VIEW = 'UPDATE_VIEW';
export const UPDATE_MATCH_IDX = 'UPDATE_MATCH_IDX';
export const UPDATE_LINKAGE = 'UPDATE_LINKAGE';

// specs for data in 'generate' mode
var domain = 'hacc';
var normalized = 'normalized';
var linkage = 'single';
var comparison = 'mass';
var massBin = 'd0';

var nClusters = 1;
const dataMode = 'read'; // 'read', 'generate'

//FIXME! need db 
//const {domain, normalized, linkage, comparison, massBin} = this.state;
var filePaths = constructFilenames({domain: domain, normalized: normalized, linkage: linkage, comparison: comparison, massBin: massBin});

export const updateNClusters = n => ({
	type: UPDATE_N_CLUSTERS,
	payload: { n }
});

export const updateView = views => ({
	type: UPDATE_VIEW,
	payload: {
		velocity_on: views.velocity_on,
    	main_branches_on: views.main_branches_on,
    	mergers_on: views.mergers_on,
    	merger_demo_on: views.merger_demo_on
	}
});

export const updateLinkage = linkage => ({
	type: UPDATE_LINKAGE,
	payload: { linkage }
});

export const updateMatchIdx = i => ({
	type: UPDATE_MATCH_IDX,
	payload: {
		idx: i
	}
});

export const fetchDataBegin = () => ({
	type: FETCH_DATA_BEGIN
});

export const fetchDataSuccess = data => ({
	type: FETCH_DATA_SUCCESS,
	payload: { data }
});

export const fetchDataFailure = error => ({
	type: FETCH_FRP_FAILURE,
	payload: { error }
})

export function fetchData() {
	//TEMP!
	var fns = {};
	fns[1] = {
		'complete': 'd0_n1_complete.json',
	};
	fns[2] = {
		'complete': 'd0_n2_complete.json',
	};
	fns[3] = {
		'complete': 'd0_n3_complete.json',
		'single': 'd0_n3_single.json'
	};
	fns[4] = {
		'complete': 'd0_n4_complete.json',
	}

	return function (dispatch) {
		var data = {};
		dispatch(fetchDataBegin());

		if (dataMode == 'read'){
    		var allData = {
    			1: {},
    			2: {},
    			3: {},
    			4: {}
    		};
			return $.when(
				$.getJSON('../temp_data/' + fns[1]['complete'], function(data) {     
	    			allData[1]['complete'] = data;
	    		}),
	    		$.getJSON('../temp_data/' + fns[2]['complete'], function(data) {
	    			allData[2]['complete'] = data;
	    		}),
	    		$.getJSON('../temp_data/' + fns[3]['complete'], function(data){
	    			allData[3]['complete'] = data;
	    		}),
	    		$.getJSON('../temp_data/' + fns[3]['single'], function(data) {
	    			allData[3]['single'] = data;
	    		}),
	    		$.getJSON('../temp_data/' + fns[4]['complete'], function(data){
	    			allData[4]['complete'] = data;
	    		})
			).then(function(){
	  			return dispatch(fetchDataSuccess(allData));
			});
		}
		else {
			return $.when(
				loadLinkage(filePaths.linkage), loadData(filePaths.sim),
				loadData(filePaths.mass),
				loadData(filePaths.time), loadData(filePaths.vel),
				loadData(filePaths.xyzPos), loadData(filePaths.xyzVel),
				loadData(filePaths.mergerTimes), loadData(filePaths.mergerRatios),
				loadData(filePaths.mergedMasses)
			).then(function(l_raw, s_raw, m_raw, t_raw, v_raw, xyzp_raw, xyzv_raw, t_mrg_raw, r_mrg_raw, m_mrg_raw){
				const l = parseLinkage(l_raw);
				const s = parseData('sim', s_raw),
					  m = parseData('mass', m_raw),
					  t = parseData('time', t_raw),
					  v = parseData('vel', v_raw),
					  xyzp = parseData('xyzPos', xyzp_raw),
					  xyzv = parseData('xyzVel', xyzv_raw),
					  t_mrg = parseData('mergerTimes', t_mrg_raw),
					  r_mrg = parseData('mergerRatios', r_mrg_raw),
					  m_mrg = parseData('mergedMasses', m_mrg_raw)

				const mainMass = getMainBranch(t,m,m),
					  mainTime = getMainBranch(t,m,t),
					  mainVel = getMainBranch(t,m,v),
					  mainXYZPos = getMainBranch(t,m,xyzp),
					  mainXYZVel = getMainBranch(t,m,xyzv);

				const timeSeries = {
					mass: m,
					time: t,
					vel: v,
					xyzPos: xyzp,
					xyzVel: xyzv,
					mainMass: mainMass,
					mainTime: mainTime,
					mainVel: mainVel,
					mainXYZPos: mainXYZPos,
					mainXYZVel: mainXYZVel,
					mergerTimes: t_mrg,
					mergerRatios: r_mrg,
					mergedMasses: m_mrg
				};

				// return an object that includes all these, 'data'
				const clusterAssignments = l.tree.getClusterAssignments(nClusters);
				console.log("clusterAssignments: ", clusterAssignments);
				const dataStats = calculateClusterRibbons({timeSeries: timeSeries, domain: domain, nClusters: nClusters, clusterAssignments: clusterAssignments});
				console.log("datastats: ", dataStats);

				//FIXME: clusterassignemnts and datastats depend on data and nClusters, which are other members of the state. 
				// therefore, they should be calculated (_____?)
				const data = {
					similarity: s,
					timeSeries: timeSeries,
					linkage: l.linkage,
					//tree: l.tree,
					clusterAssignments: clusterAssignments,
					dataStats: dataStats
				};
				writeFile(JSON.stringify(data));
				const dataStruct = {
					1: data
				}
				dispatch(fetchDataSuccess(dataStruct));
			}).catch(function(error){
	  			dispatch(fetchDataFailure(error));
	  		});
		}
	}
}

function writeFile(text){
  var textFile = null,
  makeTextFile = function (text) {
  	console.log("writing json file...");
    var data = new Blob([text], {type: 'application/json'});

    // If we are replacing a previously generated file we need to
    // manually revoke the object URL to avoid memory leaks.
    if (textFile !== null) {
      window.URL.revokeObjectURL(textFile);
    }

    textFile = window.URL.createObjectURL(data);

    // returns a URL you can use as a href
    console.log("url: ", textFile);
  };
  makeTextFile(text);
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}