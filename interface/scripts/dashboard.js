import React, { Component } from 'react';
import ReactDOM, {render} from 'react-dom';
import {connect} from 'react-redux';
import {updateNClusters} from './dataActions';
import * as d3 from 'd3';

//FIXME: put this in the layout wrapper (?)
const defaultProps = {
	//colors: ["#a6cee3","#1f78b4","#b2df8a","#33a02c","#fb9a99","#e31a1c","#fdbf6f","#ff7f00","#cab2d6","#6a3d9a","#ffff99","#b15928"], //colorbrewer categorical palette
	colors: ["#9aa0b8", "#d9bb6e", "#585537", "#808353", "#5a6f83", "#3d071f", "#85482b", "#791a11"], // 'goya' color palette from sciviscolor.org
	nClusters: 1 //FIXME!
}

export class Dashboard extends Component {
	constructor(props){
		super(props);
		this.state = {
			nClusters: 1
			//data: {},
			//timeSeries: [],
			//clusterAssignments: [],
		}

		this.drawPies = this.drawPies.bind(this);
	}
	/*
	this.timeSeries = timeSeries;
	*/

	componentDidMount(){
		if (document.getElementsByClassName('lm_item').length > 0){
			this.setState({width: parseFloat(document.getElementsByClassName('lm_item')[3].style.width.slice(0,-2))}); //FIXME
			this.setState({height: parseFloat(document.getElementsByClassName('lm_item')[3].style.height.slice(0,-2))}); //FIXME
		}
		//this.props.dispatch(updateNClusters(defaultProps.nClusters));
        window.addEventListener('resize', this._resize.bind(this));
        this._resize();
	}

	componentDidUpdate(){
		this.drawPies();
	}
		
	_resize(){
		if (document.getElementsByClassName('lm_item').length > 0){
			this.setState({width: parseFloat(document.getElementsByClassName('lm_item')[3].style.width.slice(0,-2))}); //FIXME
			this.setState({height: parseFloat(document.getElementsByClassName('lm_item')[3].style.height.slice(0,-2))}); //FIXME
		}
		this._onViewportChange({
			width: window.innerWidth,
			height: window.innerHeight
		});
	}
	_onViewportChange(viewport) {
    	this.setState({
      		viewport: {...this.state.viewport, ...viewport}
    	});
  	}

    //this.clusterIDs = clusterAssignments;
    //this.nClusters = Math.max.apply(Math, this.clusterIDs) + 1;
    
    //var heightPer = (this.height - this.xAxisHeight) / this.nClusters;

    /*
    for (i = 0; i < this.nClusters; i++){
    	this.svg.append("rect")
			.attr("class", "inner-border")
	    	.attr("width", width)
	    	.attr("height", heightPer+1)
	    	.attr("transform", "translate(0," + i*heightPer + ")");
    }
    */
    
    drawPies(){
    	const node = this.node; //render
    	d3.select(node).selectAll("*").remove();

    	const { width, height } = this.state;
        const { data, nClusters, linkage } = this.props;
    	if (typeof data !== "undefined" && '1' in data) {
	        const clusterIDs = data[nClusters][linkage].clusterAssignments;

		    var heightPer = height/nClusters;
			var pieRow = [];
			for (var n = 0; n < nClusters; n++){
				var count = 0;
				for (var j = 0; j < clusterIDs.length; j++){
					if (clusterIDs[j] == n) count++;
				}
				pieRow.push(count);
			}
			var pieData = [];
			for (var n = 0; n < pieRow.length; n++){ pieData.push(pieRow); }
				
			var colors = [];
			var opacities = [];
			for (var n = 0; n < pieData.length; n++){
				var colorRow = []; //FIXME: duplicate names D:
				var opacityRow = [];
				for (var j = 0; j < pieData[n].length; j++){
					//if (n == j) colorRow.push(defaultProps.colors[2*j%defaultProps.colors.length]);
					//else colorRow.push("gainsboro");
					colorRow.push(defaultProps.colors[2*j%defaultProps.colors.length]);
					if (n == j) opacityRow.push('1.0');
					else opacityRow.push('0.2');
				}
				colors.push(colorRow);
				opacities.push(opacityRow);
			}
			var dataTotal = pieData[0].reduce(function(a, b) { return a + b; }, 0);
				
			var pie = d3.pie()
				.sort(null)
				.value(function(d) { return d; });
				
			var pieX = width/2;
			var radius = Math.min(0.2 * heightPer,0.4*width);
			
			function pieDist(i){
				return (height - i*heightPer - (heightPer/2));
			}
			
			var idx = 0;	
			for (var n = 0; n < nClusters; n++){
			    var g = d3.select(node).append("g")
			        .attr("width", width)
			        .attr("height", (height) / nClusters)
			        .attr("transform", "translate(" + pieX + "," + pieDist(n) + ")");
			        
			    g.selectAll("path")
				    .data(pie(pieData[0]))
			        .enter().append("path")
				    .attr("d", d3.arc()
					.innerRadius(0)
					.outerRadius(radius))
				    .style("fill", function(d,i){ var color = colors[idx][i]; return color;})
				    .style("opacity", function(d,i){ var o = opacities[idx][i]; if (i == pieData.length-1) { idx++; } return o; })
				    .attr("class", "arc");
			}	
		}
	}
	render(){
    	const {width, height} = this.state;
        return <svg width={width} height={height} ref={node => this.node = node}>
        </svg>

        //var dataStats = calculateClusterRibbons(nClusters, clusterAssignments);
        //clusterView.drawClusters(dataStats, encodingOption);
        //clusterView.drawScatter(dataStats, timeSeries, clusterAssignments);
        //clusterView.drawTimeSeries(timeSeries, 1.0, clusterAssignments); //FIXME: fraction capability
    }
}

const mapStateToProps = state => ({
    data: state.items,
    loading: state.loading,
    error: state.error,
    nClusters: state.nClusters,
    views: state.views,
    linkage: state.linkage
});

const DashboardContainer = connect(mapStateToProps)(Dashboard);

export default DashboardContainer;

// Dashboard.prototype.update = function(n, timeSeries, clusterAssignments, option){
// 	console.log("update dash: ", n);
//     this.timeSeries = timeSeries;
//     this.nClusters = n;
//     this.clusterIDs = clusterAssignments;
    
//     this.svg.selectAll("g").remove();
    
//     this.svg.selectAll(".inner-border").remove();
//     var heightPer = (this.height - this.xAxisHeight)/n;
// 	for (i = 0; i < this.nClusters; i++){
// 		this.svg.append("rect")
// 			.attr("class", "inner-border")
// 			.attr("width", width)
// 			.attr("height", heightPer+1)
// 			.attr("transform", "translate(0," + i*heightPer + ")");
// 	}
//     if (option == 'none') { }
//     else if (option == 'pies') { this.drawPies(); }
//     else if (option == 'maps') { this.drawMaps(); }
//     else if (option == 'space') { this.drawSpace(); }
// }

	// pieGroup.append("text")
	// 	.text(function(d,i){ return d[i]})
	// 	.attr("font-family", "sans-serif")
	// 	.attr("font-size", "12px")
	// 	.attr("fill", "black")
	// 	.attr("transform", "translate(" + -0.4*this.width + ",0)");
	// pieGroup.append("text")
	// 	.text(function(d,i){ return 100*d[i]/dataTotal + "%"; })
	// 	.attr("font-family", "sans-serif")
	// 	.attr("font-size", "12px")
	// 	.attr("fill", "black")
	// 	.attr("transform", "translate(" + -0.4*this.width + ",14)");

// Dashboard.prototype.drawHist = function(){
//     //FIXME: do this for real, not-on-deadline :)
//     data = [0.433388974336, 0.515710717104, 0.519267490443, 0.520680350404, 0.527043537959, 0.527816154055, 0.529442816014, 0.535964701367, 0.536803524571, 0.537735348687, 0.538763845482, 0.540455274934, 0.545362235895, 0.552579632725, 0.555085059529, 0.556857674692, 0.557606134617, 0.559204640492, 0.559512083274, 0.571068973314, 0.572283865159, 0.57296220251, 0.574289977492, 0.578207782183, 0.579224440445, 0.579734457827, 0.579798126332, 0.581927287678, 0.588482743183, 0.59106482603, 0.596026590929, 0.601423580672, 0.603657137756, 0.603833371131, 0.606155385493, 0.611701795833, 0.612291559071, 0.612833848095, 0.614655223618, 0.617713151307, 0.618475817963, 0.622898669311, 0.624090332172, 0.624134269456, 0.626200937913, 0.62705670098, 0.628290476229, 0.629183668634, 0.630585460428, 0.632245152611, 0.63231286146, 0.632893224393, 0.632943118688, 0.633942246716, 0.63715299035, 0.637745795276, 0.63794038408, 0.638512497259, 0.639693250945, 0.639837360801, 0.640079493025, 0.640246518116, 0.641983376541, 0.645188936986, 0.6453038255, 0.645359881202, 0.645688885448, 0.646185683654, 0.648718140482, 0.650190636366, 0.650734094805, 0.652206023313, 0.655679918148, 0.655901738327, 0.655971500249, 0.656544301408, 0.656924453627, 0.657652250994, 0.660250247834, 0.660916237299, 0.661643711096, 0.662145846642, 0.666030650829, 0.666184727326, 0.666704931118, 0.666906218289, 0.670484608694, 0.671454246367, 0.673058507083, 0.674119162768, 0.674565316793, 0.674607109873, 0.676645838066, 0.677721712822, 0.67832738975, 0.678583618342, 0.680712094732, 0.682361846079, 0.682388268989, 0.682603622056, 0.684873959062, 0.685428966157, 0.686224706468, 0.686959656752, 0.687642504657, 0.691635024003, 0.692446147528, 0.692608662314, 0.69327713879, 0.694959802861, 0.695347131388, 0.697189495026, 0.697795153686, 0.699206080955, 0.700459823566, 0.700649034495, 0.701282055487, 0.702299138733, 0.70252466334, 0.702882015434, 0.70321506991, 0.70378568361, 0.70390197321, 0.704271374898, 0.704768011229, 0.705220900346, 0.70549767788, 0.705838485662, 0.706903568134, 0.707985554581, 0.709713093064, 0.711262610158, 0.713039950714, 0.713877058894, 0.715286235188, 0.716746406852, 0.718412562382, 0.719153674763, 0.719324625811, 0.719816871314, 0.721516255181, 0.722226528441, 0.723196945392, 0.724172039785, 0.726608112956, 0.727059313315, 0.727507572699, 0.72856353298, 0.729483994705, 0.729832252252, 0.729949057494, 0.731178594382, 0.73175053743, 0.732062397389, 0.732401962478, 0.73296343438, 0.733365099778, 0.734920566925, 0.736279221518, 0.737665975373, 0.737678247687, 0.740129278068, 0.742079303365, 0.742359152394, 0.742470910175, 0.744149439167, 0.744880281975, 0.746022862898, 0.746139910239, 0.748347857688, 0.748907421934, 0.74964429553, 0.751470304268, 0.752450611001, 0.753086654422, 0.753668563631, 0.754698747961, 0.755376223758, 0.755503942324, 0.755854695002, 0.756839160146, 0.75873489381, 0.759090913057, 0.759811718922, 0.760949817323, 0.762061879584, 0.763643841058, 0.76399274652, 0.763992787369, 0.764239145829, 0.765445399782, 0.767765673553, 0.768875069624, 0.771245620121, 0.771280810899, 0.77240438613, 0.773058483606, 0.773883279561, 0.774136823257, 0.774713308849, 0.776002122921, 0.776545952977, 0.777117472973, 0.777249892045, 0.777321348137, 0.778303518467, 0.778711269578, 0.778776281929, 0.7788072078, 0.779140141568, 0.780999000546, 0.781409190466, 0.781705266012, 0.78312884438, 0.783158097518, 0.783216458181, 0.783236287721, 0.784028466958, 0.784466127651, 0.784877908586, 0.785912014617, 0.78633472016, 0.786450689735, 0.787301057553, 0.787730050114, 0.788294963037, 0.788478547155, 0.78871883116, 0.789860297397, 0.791420738345, 0.791853603432, 0.792050179593, 0.793779570269, 0.794307085197, 0.794606708393, 0.795897403436, 0.796139891236, 0.796451640739, 0.796603842858, 0.797453650152, 0.79845587259, 0.798607282917, 0.798651693924, 0.798819595583, 0.799479146156, 0.799968804793, 0.800742318809, 0.8008191164, 0.801205117038, 0.802039289984, 0.802172604154, 0.804638929316, 0.80619816422, 0.806689048574, 0.80671242839, 0.808033495201, 0.808083709216, 0.80979856579, 0.809855977699, 0.809972208592, 0.810018130344, 0.81031941504, 0.810384388004, 0.810473852652, 0.81051537984, 0.8111914476, 0.811340916488, 0.811447499847, 0.812427560787, 0.812504270968, 0.814827294341, 0.818351056451, 0.819152427038, 0.819573234957, 0.819652001083, 0.820669504213, 0.821369413197, 0.822639137107, 0.823343914853, 0.824178503623, 0.824508335607, 0.824734033569, 0.827893459131, 0.828115950352, 0.828305190884, 0.828313835114, 0.828556454017, 0.829249473182, 0.829629117422, 0.830248967012, 0.830545404193, 0.8312477975, 0.832529642236, 0.832708328555, 0.833431670775, 0.834000545991, 0.834398889798, 0.836207608101, 0.836241360219, 0.836266106367, 0.837069626993, 0.837980907499, 0.838432696871, 0.838554280649, 0.838814359332, 0.84094055063, 0.841279481915, 0.841412262272, 0.841670261865, 0.842814829113, 0.843376299049, 0.844408529308, 0.844776607858, 0.844938515378, 0.845719828283, 0.845767513902, 0.846074463825, 0.846283685012, 0.846443501622, 0.84653901893, 0.847679875427, 0.848805217417, 0.850948075628, 0.85183616512, 0.852971308583, 0.854053575539, 0.854350386845, 0.854591049519, 0.855757287684, 0.855842423573, 0.856258167346, 0.857042594741, 0.857106301578, 0.857685472709, 0.860011528204, 0.861080262782, 0.861304548903, 0.861774388762, 0.863398210858, 0.864923727834, 0.864991367506, 0.865212500502, 0.865581716315, 0.866794039616, 0.867156074387, 0.86809408699, 0.869864364125, 0.870858565753, 0.871624528922, 0.871772756849, 0.872123786933, 0.87229305581, 0.872440458263, 0.87346718868, 0.873555970637, 0.873617530682, 0.873773922437, 0.874431979088, 0.876111673805, 0.876236503829, 0.878203782745, 0.878998828604, 0.879062089043, 0.879408199769, 0.879449900336, 0.882315398571, 0.882897924788, 0.885209787437, 0.885816944153, 0.886131925098, 0.886778346795, 0.887184906917, 0.888075306716, 0.88825524056, 0.888869343869, 0.891629785337, 0.891940949167, 0.893195538073, 0.895273711944, 0.895357672492, 0.895457023103, 0.89626498653, 0.89677861257, 0.897761395222, 0.898780113432, 0.900538997405, 0.900717453953, 0.900762519916, 0.901389117525, 0.905698932429, 0.906367623018, 0.906930972819, 0.908994045218, 0.909153904662, 0.909180759122, 0.909876839038, 0.910753989989, 0.911762446802, 0.912606199254, 0.913295862297, 0.915231605191, 0.916367091292, 0.918096885462, 0.91945545836, 0.920466964754, 0.921461064355, 0.924211029105, 0.925611099382, 0.927553445479, 0.92788300431, 0.928163488697, 0.928580675009, 0.929847021198, 0.930053778128, 0.932552082268, 0.933339649878, 0.933480999199, 0.935127679042, 0.940408862306, 0.947405832472, 0.947844427774, 0.948214105693, 0.949462724226, 0.950482200402, 0.951871057877, 0.954278541467, 0.955522928146, 0.95686228758, 0.958167922498, 0.958489700986, 0.959613978119, 0.959622783948, 0.960585520587, 0.960810851286, 0.962874935658, 0.963580365435, 0.964849967116, 0.967840549298, 0.96848609021, 0.968587531636, 0.96942162284, 0.969781544725, 0.971305641098, 0.97221024364, 0.973120975034, 0.973735209345, 0.977236822427, 0.977345855725, 0.977840010458, 0.985358532783, 0.987996353641, 0.989782157928, 0.990006807161, 0.990987553421, 0.991371761737, 0.994755954301, 1.00302473805, 1.00505844868, 1.00811858083, 1.01216905086, 1.0122382419, 1.01354489971, 1.01771294415, 1.02040810368, 1.02517012485, 1.03004509704, 1.03487852464, 1.03569065332, 1.03719968491, 1.03737020515, 1.04175523774, 1.04692481037, 1.04753444079, 1.04879180495, 1.05402899173, 1.05998038048, 1.06950284729, 1.0730694568, 1.07453871191, 1.08615150384, 1.08957464133, 1.09416053476, 1.09521507083, 1.10745641224, 1.10787646209, 1.11486604388, 1.11565976225, 1.1198124193, 1.12086910045, 1.13151187749, 1.13876365874, 1.14419085604, 1.14656934288, 1.15412913195, 1.15424768238, 1.1589578649, 1.16246823517, 1.16994170044, 1.17495832874, 1.18290140673, 1.2008543297, 1.20162530486, 1.20303049408, 1.20349449439, 1.20819198965, 1.20863006199, 1.21396870227, 1.21762450586, 1.21903536328, 1.2255552432, 1.23189989331, 1.23278530263, 1.23918801042, 1.2468993203, 1.25311462061, 1.26068882819, 1.2639249833, 1.27622958257, 1.28762679145, 1.29213942039, 1.29900472749, 1.31950231247, 1.34258393579, 1.34623095417, 1.35319206022, 1.36272225364, 1.37925435082, 1.47897603073, 1.48668198632, 1.49356527659, 1.53339168288, 1.77630166138, 1.78821827914, 1.80931136249, 1.83085420725, 1.83669676525, 1.90112044548, 3.11020213759];
    
//     var x = d3.scaleLinear()
//         .domain([0,d3.max(data)])
//         .range([10,this.width-10])
        
//     var heightPer = this.height/data.length;
        
//     var g = this.svg.append("g")
// 	    .attr("width", this.width)
// 	    .attr("height", this.height);
        
//     g.selectAll(".chart")
//             .data(data)
//         .enter().append("rect")
//             .attr("width", function(d) { return x(d); })
//             .attr("height", heightPer)
//             .attr("transform", function(d,i){ return ("translate(5," + i*heightPer + ")"); })
//             .attr("fill", "#ccc");
// }

// Dashboard.prototype.drawMaps = function(){
// 	var pointData = [];
// 	var lat = this.timeSeries["lat"];
// 	var lon = this.timeSeries["lon"];
// 	var flatLat = [].concat.apply([],lat);
// 	var flatLon = [].concat.apply([],lon);
	
// 	var maxLat = Math.max.apply(Math, flatLat);
// 	var minLat = Math.min.apply(Math, flatLat);
// 	var maxLon = Math.max.apply(Math, flatLon);
// 	var minLon = Math.min.apply(Math, flatLon);
	
// 	var heightPer = Math.min((this.height-this.xAxisHeight)/this.nClusters, this.width-6);
// 	var width = this.width;
	
// 	var projection = d3.geoAlbers()
// 		.fitExtent([[0-heightPer, 0-heightPer], [heightPer, heightPer]], usa_json);
		
// 	var upperRight = projection([maxLon,maxLat]);
// 	var lowerLeft = projection([minLon,minLat]);
// 	var range = Math.max(Math.abs(upperRight[0]-lowerLeft[0]), Math.abs(upperRight[1]-lowerLeft[1]));
// 	var scale = 1.0;//heightPer/range;
						
// 	var center = projection([0.5*(maxLon+minLon),0.5*(maxLat+minLat)]);
// 	var currentCenter = [0.5*heightPer, 0.5*heightPer];
					
// 	for (i = 0; i < flatLat.length; i++){
// 		var coord = {};
// 		coord.d = projection([flatLon[i], flatLat[i]]);
// 		coord.idx = this.clusterIDs[i];
// 		coord.color = this.colors[2*this.clusterIDs[i]+1];
// 		pointData.push(coord);
// 	}
			
// 	var geoPath = d3.geoPath()
// 		.projection(projection);
		
// 	for (n = 0; n < this.nClusters; n++){ //FIXME: there is a better way!
// 		var g = this.svg.append("g")
// 			.attr("width", width)
// 			.attr("height", heightPer)
// 			.attr("transform", "translate(" + (currentCenter[0]-center[0]) + "," + (n*heightPer - (center[1]-currentCenter[1])) + "),scale(1.0)");
	
// 		g.selectAll("path")
// 			.data(usa_json.features)
// 			.enter()
// 			.append("path")
// 			.attr("fill", "#ccc")
// 			.attr("d", geoPath);
// 	}	
	
// 	var height = this.height - this.xAxisHeight;
	
// 	function translate(d){
// 		var i = d.idx+1;
// 		var y = height - (i)*(heightPer) - (center[1] - currentCenter[1]);
// 		return "translate(" + (currentCenter[0]-center[0]) + "," + y + "),scale(1.0)";
// 	}
	
// 	var g = this.svg.append("g")
// 		.attr("width", this.width)
// 		.attr("height", this.height - this.xAxisHeight);
			
// 	g.selectAll("circle")
// 		.data(pointData)
// 		.enter()
// 		.append("circle")
// 		.attr("cx", function(d) { return d.d[0]; })
// 		.attr("cy", function(d) { return d.d[1]; })
// 		.attr("r", "2px")
// 		.attr("fill", function(d) { return d.color; })
// 		.attr("transform", translate);

// }

// Dashboard.prototype.drawSpace = function(){
// 	var width = this.width/2;
// 	var heightPer = (this.height-this.xAxisHeight)/this.nClusters;
// 	var height = this.height-this.xAxisHeight;
// 	var clusterIDs = this.clusterIDs;
	
// 	var timeSeries = this.timeSeries;
//     function getPosRange(){
//     	var minX = 1000000000000, maxX = 0, minY = 1000000000000, maxY = 0, minZ = 1000000000000, maxZ = 0;
//     	for (i = 0; i < timeSeries['xyzPos'].length; i++){
//     		for (t = 0; t < timeSeries['xyzPos'][i].length; t++){
//     			if (timeSeries['xyzPos'][i][t][0] > maxX) { maxX = timeSeries['xyzPos'][i][t][0]; }
//     			if (timeSeries['xyzPos'][i][t][0] < minX) { minX = timeSeries['xyzPos'][i][t][0]; }
//     			if (timeSeries['xyzPos'][i][t][1] > maxY) { maxY = timeSeries['xyzPos'][i][t][1]; }
//     			if (timeSeries['xyzPos'][i][t][1] < minY) { minY = timeSeries['xyzPos'][i][t][1]; }
//     			if (timeSeries['xyzPos'][i][t][2] > maxZ) { maxZ = timeSeries['xyzPos'][i][t][2]; }
//     			if (timeSeries['xyzPos'][i][t][2] < minZ) { minZ = timeSeries['xyzPos'][i][t][2]; }
//     		}
//     	}
//     	//var bounds = {'minX': minX, 'maxX': maxX, 'minY': minY, 'maxY': maxY, 'minZ': minZ, 'maxZ': maxZ};
//     	//temp: to ensure even scaling
//     	var bounds = {'minX': -10, 'maxX': 150, 'minY': -10, 'maxY': 150, 'minZ': -10, 'maxZ': 150};
//     	return bounds;
//     }
    
//     var bounds = getPosRange();
	
// 	var j = 10, scale = 2.2, scatter = [], yLine = [], xGrid = [], beta = 0, alpha = 0, key = function(d){ return d.id; }, startAngle = 0;// Math.PI/4;
//     var svg    = this.svg.call(d3.drag().on('drag', dragged).on('start', dragStart).on('end', dragEnd)).append('g');
//     var color  = d3.scaleSequential(d3.interpolatePurples);
//     var mx, my, mouseX, mouseY;
    
//     function origin(i){
//         return [width, ((i*heightPer)+width)]; 
//     }
    
//     function createBBox(bounds){
//     	bbox = [];
//     	bbox.push([[bounds.minX, bounds.minY, bounds.minZ], [bounds.maxX, bounds.minY, bounds.minZ]]);
//     	bbox.push([[bounds.minX, bounds.minY, bounds.minZ], [bounds.minX, bounds.maxY, bounds.minZ]]);
//     	bbox.push([[bounds.minX, bounds.minY, bounds.minZ], [bounds.minX, bounds.minY, bounds.maxZ]]);
//     	bbox.push([[bounds.maxX, bounds.minY, bounds.minZ], [bounds.maxX, bounds.maxY, bounds.minZ]]);
//     	bbox.push([[bounds.maxX, bounds.minY, bounds.minZ], [bounds.maxX, bounds.minY, bounds.maxZ]]);
//     	bbox.push([[bounds.maxX, bounds.maxY, bounds.minZ], [bounds.minX, bounds.maxY, bounds.minZ]]);
//     	bbox.push([[bounds.maxX, bounds.minY, bounds.maxZ], [bounds.minX, bounds.minY, bounds.maxZ]]);
//     	bbox.push([[bounds.minX, bounds.maxY, bounds.minZ], [bounds.minX, bounds.maxY, bounds.maxZ]]);
//     	bbox.push([[bounds.minX, bounds.minY, bounds.maxZ], [bounds.minX, bounds.maxY, bounds.maxZ]]);
//     	bbox.push([[bounds.minX, bounds.maxY, bounds.maxZ], [bounds.maxX, bounds.maxY, bounds.maxZ]]);
//     	bbox.push([[bounds.maxX, bounds.maxY, bounds.maxZ], [bounds.maxX, bounds.minY, bounds.maxZ]]);
//     	bbox.push([[bounds.maxX, bounds.maxY, bounds.maxZ], [bounds.maxX, bounds.maxY, bounds.minZ]]);
    	
//     	return bbox;
//     }

//     var xScale = d3.scaleLinear()
//     				.domain([bounds['minX'], bounds['maxX']])
//     				.range([-1,1]);
    				
//     var yScale = d3.scaleLinear()
//     				.domain([bounds['minY'], bounds['maxY']])
//     				.range([-1,1]);
    				
//    var zScale = d3.scaleLinear()
//     				.domain([bounds['minZ'], bounds['maxZ']])
//     				.range([-1,1]);

//     var point3d = d3._3d()
//         .x(function(d){ return d.x; })
//         .y(function(d){ return d.y; })
//         .z(function(d){ return d.z; })
//         .origin(origin(0))
//         .rotateY( startAngle)
//         .rotateX(-startAngle)
//         .scale(scale);  

//     var _3d = d3._3d()
//         .scale(scale)
//         .origin(origin(0))
//         .shape('LINE_STRIP')
//         .rotateX(-startAngle)
//         .rotateY(startAngle);
        
//     function processData(data, tt){
//         var points = svg.selectAll('circle').data(data[1], key);

//         points
//             .enter()
//             .append('circle')
//             .attr('class', '_3d')
//             .attr('opacity', 0)
//             .attr('cx', posPointX)
//             .attr('cy', posPointY)
//             .merge(points)
//             .transition().duration(tt)
//             .attr('r', 4)
//             .attr('fill', function(d){ return color(d.m); })
//             .attr('opacity', 1)
//             .attr('cx', posPointX)
//             .attr('cy', posPointY);

//         points.exit().remove();
        
//         var linesStrip = svg.selectAll('path').data(data[0], key);
//         linesStrip
//             .enter()
//             .append('path')
//             .attr('class', '_3d')
//             .merge(linesStrip)
//             .attr('fill', 'none')
//             .attr('stroke', 'gray')
//             .attr('stroke-width', 0.5)
//             .attr('stroke-opacity', 1.0)
//             .sort(function(a, b){ return b[0].rotated.z - a[0].rotated.z; })
//             .attr('d', _3d.draw);
//             linesStrip.exit().remove();
        	
//         d3.selectAll('._3d').sort(d3._3d().sort);
//     }

//     function posPointX(d){
//         return d.projected.x;
//     }

//     function posPointY(d){
//         return d.projected.y;
//     }

// 	function init(){
//         var cnt = 0;
//         xGrid = [], scatter = [], yLine = [], paths = [];
//         ID = 0;
// 		for(i = 0; i < timeSeries['mainXYZPos'].length; i++){
// 			path = [];
// 			for(var t = 0; t < timeSeries['mainXYZPos'][i].length; t++){
// 				var mScale = d3.scaleLog().domain([Math.min.apply(Math,timeSeries['mainMass'][i]),Math.max.apply(Math,timeSeries['mainMass'][i])]).range([-0.5,1.0]);
// 				var entry = {x: timeSeries['mainXYZPos'][i][t][0], y: timeSeries['mainXYZPos'][i][t][1], z: timeSeries['mainXYZPos'][i][t][2],
// 				m: mScale(timeSeries['mainMass'][i][t]), cluster: clusterIDs[i], id: 'point_' + cnt++};
// 				if (mScale(timeSeries['mainMass'][i][t]) > 0.0 && clusterIDs[i] == ID) { scatter.push(entry); }
				
// 				if (mScale(timeSeries['mainMass'][i][t]) < 1.0) { path.push([timeSeries['mainXYZPos'][i][t][0], timeSeries['mainXYZPos'][i][t][1], timeSeries['mainXYZPos'][i][t][2]]); }
// 			}
// 			//if (clusterIDs[i]== ID) { paths.push(path); }
// 		}
// 		var bbox = createBBox(bounds);
// 		for (i = 0; i < 12; i++){
// 		    paths.push(bbox[i]);
// 		}
// 		console.log("paths: ", paths);
// 		console.log("scatter: ", scatter);
//         var data = [
//             _3d(paths),
//             point3d(scatter),
//             []//_3d(bbox)
//         ];
        
//         processData(data, 1000);
//     }

//     function dragStart(){
//         mx = d3.event.x;
//         my = d3.event.y;
//     }

//     function dragged(){
//         mouseX = mouseX || 0;
//         mouseY = mouseY || 0;
//         beta   = (d3.event.x - mx + mouseX) * Math.PI / 230 ;
//         alpha  = (d3.event.y - my + mouseY) * Math.PI / 230  * (-1);
//         var data = [
//             _3d.rotateY(beta + startAngle).rotateX(alpha - startAngle)(paths),
//             point3d.rotateY(beta + startAngle).rotateX(alpha - startAngle)(scatter),
//             _3d.rotateY(beta + startAngle).rotateX(alpha - startAngle)(bbox),
//         ];
//         processData(data, 0);
//     }

//     function dragEnd(){
//         mouseX = d3.event.x - mx + mouseX;
//         mouseY = d3.event.y - my + mouseY;
//     }

//     d3.selectAll('button').on('click', init);

//     init();
// }